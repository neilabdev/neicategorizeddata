# NEICategorizedData

[![CI Status](http://img.shields.io/travis/James Whitfield/NEICategorizedData.svg?style=flat)](https://travis-ci.org/James Whitfield/NEICategorizedData)
[![Version](https://img.shields.io/cocoapods/v/NEICategorizedData.svg?style=flat)](http://cocoapods.org/pods/NEICategorizedData)
[![License](https://img.shields.io/cocoapods/l/NEICategorizedData.svg?style=flat)](http://cocoapods.org/pods/NEICategorizedData)
[![Platform](https://img.shields.io/cocoapods/p/NEICategorizedData.svg?style=flat)](http://cocoapods.org/pods/NEICategorizedData)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NEICategorizedData is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NEICategorizedData"
```

## Author

James Whitfield, jwhitfield@neilab.com

## License

NEICategorizedData is available under the MIT license. See the LICENSE file for more info.
