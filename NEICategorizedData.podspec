#
# Be sure to run `pod lib lint NEICategorizedData.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NEICategorizedData'
  s.version          = '0.1.3'
  s.summary          = 'NEICategorizedData allows you to easily categorize data to be loaded in table & collection views'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
NEICategorizedData allows you to easily categorize data to be loaded in table & collection views by allowing you to
specify section, the object, and a representation of its type for easy querying.
                       DESC

  s.homepage         = 'git@bitbucket.org:ic2media/neicategorizeddata.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'James Whitfield' => 'jwhitfield@neilab.com' }
  s.source           = { :git => 'git@bitbucket.org:ic2media/neicategorizeddata.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.module_map="NEICategorizedData/NEICategorizedData.modulemap"
  s.source_files = 'NEICategorizedData/Classes/**/*'
  
  # s.resource_bundles = {
  #   'NEICategorizedData' => ['NEICategorizedData/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
