//
// Created by James Whitfield on 9/30/15.
// Copyright (c) 2015 NEiLAB, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NEICategorizedRow : NSObject
@property (nonatomic, readonly) id data;
@property (nonatomic, readonly) NSString *type;
@property (nonatomic, readonly) NSString *section;
- (instancetype)initWithData:(id)data type:(NSString *)type;
+ (instancetype)rowWithData:(id)data type:(NSString *)type;
@end

@interface NEICategorizedData : NSObject

- (void) clear;
#pragma mark -
- (NSInteger) numberOfSections;
- (NSInteger) numberOfRowsInSection:(NSUInteger)  section;
- (NSString *)nameAtSection: (NSUInteger) section ;
#pragma mark - Manipulate Data
- (NEICategorizedRow *) addRow: (id)row  type: (NSString*) type section:(NSString*) sectionName;
- (void) addRows: (NSArray *) rowz type: (NSString*) type section:(NSString*) sectionName;
- (void) moveRow:(NEICategorizedRow *) tableViewRow toSectionNamed: (NSString *)sectionName;
- (void) deleteRow:(NEICategorizedRow *) tableViewRow;
- (void) importData: (NEICategorizedData *) data;

#pragma mark - Indexed Subscriptiong

- (id)objectAtIndexedSubscript:(NSUInteger)idx;
- (void)setObject:(id)anObject atIndexedSubscript:(NSUInteger)index;

- (id)objectForKeyedSubscript:(id)key;
- (void)setObject:(id)object forKeyedSubscript:(id<NSCopying>)aKey;
@end