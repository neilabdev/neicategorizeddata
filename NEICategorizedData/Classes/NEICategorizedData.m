//
// Created by James Whitfield on 9/30/15.
// Copyright (c) 2015 NEiLAB, LLC. All rights reserved.
//

#import "NEICategorizedData.h"


@interface  NEICategorizedRow()
@property (nonatomic,copy) NSString *sectionName;
@end

@implementation NEICategorizedRow

- (instancetype)initWithData:(id)data type:(NSString *)type {
    self = [super init];
    if (self) {
        _data = data;
        _type = [type copy];
    }

    return self;
}

- (NSString *)section {
    return _sectionName;
}


+ (instancetype)rowWithData:(id)data type:(NSString *)type {
    return [[self alloc] initWithData:data type:type];
}

@end

@interface NEICategorizedData ()
@property(nonatomic, retain) NSMutableArray *sections;
@property(nonatomic, retain) NSMutableDictionary *rows;
@end

@implementation NEICategorizedData {}

#pragma mark - Query Data


- (instancetype)init {
    self = [super init];
    if (self) {
        self.sections = [NSMutableArray arrayWithCapacity:1];
        self.rows = [NSMutableDictionary dictionaryWithCapacity:1];
    }

    return self;
}

- (NSInteger) numberOfSections {
    return [self.sections count];
}

- (NSInteger) numberOfRowsInSection:(NSUInteger) section {
    NSString *sectionName = (NSString*)self.sections[section]; //[self.sections objectAtIndex: section];
    NSArray *sectionRows = self.rows[sectionName];
    return [sectionRows count];
}


#pragma mark - Manipulate Data

- (NEICategorizedRow *) addRow: (id)row  type: (NSString*) type section:(NSString*) sectionName {
    NEICategorizedRow *categorizedRow = [NEICategorizedRow rowWithData:row type:type];
    [self addRow: categorizedRow section: sectionName];
    return categorizedRow;
}

- (void) addRows: (NSArray *) rowz type: (NSString*) type section:(NSString*) sectionName {
    for(id row in rowz) {
        [self addRow:row type:type section:sectionName];
    }
}

- (void) addRow:(NEICategorizedRow *) tableViewRow section: (NSString *)sectionName {
    NSMutableArray *sectionRows = self.rows[sectionName];

    if(![self.sections containsObject:sectionName])
        [self.sections addObject:sectionName];

    if(!sectionRows)  {
        self.rows[sectionName] = [NSMutableArray arrayWithObject:tableViewRow];
    } else {
        [sectionRows addObject:tableViewRow];
    }

    tableViewRow.sectionName = sectionName;
    //TODO: Add indexPath to row
}

- (void) moveRow:(NEICategorizedRow *) tableViewRow toSectionNamed: (NSString *)sectionName {
    //  NSMutableArray *oldSection = self.rows[tableViewRow.section];
    //  NSMutableArray *newSection = self.rows[sectionName];
    //  [oldSection removeObject:tableViewRow];
    [self deleteRow:tableViewRow];
    [self addRow:tableViewRow section:sectionName];
}

- (void) deleteRow:(NEICategorizedRow *) tableViewRow {
    NSMutableArray *oldSection = self.rows[tableViewRow.section];
    [oldSection removeObject:tableViewRow];
    tableViewRow.sectionName = nil;
    //add deleteAction to index path
}

- (void) importData: (NEICategorizedData *) data {
    [self.sections addObjectsFromArray:data.sections];
    [self.rows addEntriesFromDictionary:data.rows];
}

- (void) clear {
    [self.sections removeAllObjects];
    [self.rows removeAllObjects];
}

- (NSString *)nameAtSection: (NSUInteger) section { //todo: rename
    NSString *sectionName = [self.sections objectAtIndex:section];
    return sectionName;
}

#pragma mark - Subscripting
- (id)objectAtIndexedSubscript:(NSUInteger)idx{
    NSString  *sectionName = self.sections[idx];
    return self.rows[sectionName];
}

- (void)setObject:(id)anObject atIndexedSubscript:(NSUInteger)index {
    NSString  *sectionName = self.sections[index];
    self.rows[sectionName] = anObject;
}

- (id)objectForKeyedSubscript:(id)key {
    return self.rows[key];
}

- (void)setObject:(id)object forKeyedSubscript:(id<NSCopying>)aKey{
    self.rows[aKey] = object;
}
@end