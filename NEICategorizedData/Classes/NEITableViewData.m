//
// Created by James Whitfield on 9/18/15.
// Copyright (c) 2015 NEiLAB, LLC. All rights reserved.
#import "NEITableViewData.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunicode-whitespace"



@interface NEITableViewData ()
@property(nonatomic, retain) NSMutableArray *sections;
@property(nonatomic, retain) NSMutableDictionary *rows;
@property(nonatomic, retain) NSMutableDictionary *headers;
@property(nonatomic, retain) NSMutableDictionary *footers;
@end

@implementation NEITableViewData {}

- (instancetype)init {
    if (self = [super init]) {
        self.headers = [NSMutableDictionary dictionaryWithCapacity:1];
        self.footers = [NSMutableDictionary dictionaryWithCapacity:1];
    }
    return self;
}

- (void) beginUpdatesInTableView:(UITableView*)tableView actions: (IC2TableViewBlock) block {
    [tableView beginUpdates];
    block(tableView,self);
    [tableView endUpdates];
}

- (void) reloadDataInTableView:(UITableView*) tableView actions: (IC2TableViewBlock) block {
    [self clear];
    block(tableView,self);
    [tableView reloadData];
}

- (void) updateDataInTableView:(UITableView*) tableView actions: (IC2TableViewBlock) block {
    block(tableView,self);
    [tableView reloadData];
}

-(NEICategorizedRow*) dataForRowAtIndexPath: (NSIndexPath *) indexPath {
    return [self dataForRow:indexPath.row section:indexPath.section];
}

-(NEICategorizedRow*) dataForRow: (NSUInteger) row section: (NSUInteger ) section{
    NSString *sectionName = [self.sections objectAtIndex:section];
    NSArray *sectionRows = self.rows[sectionName];
    NEICategorizedRow* dataRow = (NEICategorizedRow*)[sectionRows objectAtIndex:row];
    return dataRow;
}

- (NSString*) titleForHeaderInSection: (NSInteger) section {
    NSString *sectionName = [self.sections objectAtIndex:section];
    NSString *headerName = self.headers[sectionName];
    return headerName ;
}

- (NSString*) titleForFooterInSection: (NSInteger) section {
    NSString *sectionName = [self.sections objectAtIndex:section];
    NSString *footerName = self.footers[sectionName];
    return footerName;
}

- (void) setTitle: (NSString*) sectionTitle forHeaderInSection: (NSString*) section {
    self.headers[section] = sectionTitle;
}

- (void) setTitle: (NSString*) sectionTitle forFooterInSection: (NSString*) section {
    self.footers[section] = sectionTitle;
}


- (void) reset {
    [self.sections removeAllObjects];
    [self.rows removeAllObjects];
    [self.headers removeAllObjects];
    [self.footers removeAllObjects];
}

@end

#pragma clang diagnostic pop