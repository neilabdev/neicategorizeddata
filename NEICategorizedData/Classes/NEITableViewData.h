//
// Created by James Whitfield on 9/18/15.
// Copyright (c) 2015 NEiLAB, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NEICategorizedData.h"
@class NEITableViewData;

typedef void (^IC2TableViewBlock) (UITableView* tableView, NEITableViewData *tableViewData );


@interface NEITableViewData : NEICategorizedData
- (void) setTitle: (NSString*) sectionTitle forHeaderInSection: (NSString*) section;
#pragma mark - Query Data
- (NSString*) titleForHeaderInSection: (NSInteger) section;
- (NSString*) titleForFooterInSection: (NSInteger) section;

-(NEICategorizedRow*) dataForRowAtIndexPath: (NSIndexPath *) indexPath;
-(NEICategorizedRow*) dataForRow: (NSUInteger) row section: (NSUInteger ) section;

#pragma mark - TableView updates

- (void) beginUpdatesInTableView:(UITableView*)tableView actions: (IC2TableViewBlock) block;
- (void) reloadDataInTableView:(UITableView*) tableView actions: (IC2TableViewBlock) block;
- (void) updateDataInTableView:(UITableView*) tableView actions: (IC2TableViewBlock) block;
- (void) reset;
@end

