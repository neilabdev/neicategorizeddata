//
//  NEIAppDelegate.h
//  NEICategorizedData
//
//  Created by James Whitfield on 05/22/2016.
//  Copyright (c) 2016 James Whitfield. All rights reserved.
//

@import UIKit;

@interface NEIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
